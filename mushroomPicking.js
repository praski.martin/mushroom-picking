var mushrooms = ["borowik","kania","kozlarz","maslak","rydz","muchomor"];
var table_length = mushrooms.length;
var i;
var draw;
var mushroom ="";
var change = document.getElementById("myBtn");
var info = document.getElementById("info");
var basket = document.getElementById("mushrooms");
var how_many_gathered = 0;

function play() {
    for ( i = 0 ; i < 1 ; i++){
        draw = Math.floor(Math.random() * mushrooms.length);
        mushroom += "<li>" + mushrooms[draw];
        if (mushrooms[draw] === "muchomor") {
            change.innerHTML="Zerwano muchomora KONIEC GRY!";
            change.disabled = true;
            change.classList.remove("btn-success");
            change.classList.add("btn-danger");
            info.classList.remove("d-none");
            break;
        }else{
            change.innerHTML="Zbieraj dalej !";
        }
    }
    mushroom += "</ul>";
    basket.innerHTML=mushroom;
    how_many_gathered++;
    if (how_many_gathered===table_length - 1 && mushrooms[draw] !== "muchomor"){
        change.innerHTML="Grtulacje ! Nie natrafiono na muchomora !";
        change.disabled = true;
        info.classList.remove("d-none");
    }
    mushrooms.splice(draw,1);
}

function playAgain() {
    info.classList.add("d-none");
    basket.innerHTML = "brak grzybów";
    change.classList.remove("btn-danger");
    change.classList.add("btn-success");
    change.disabled = false;
    change.innerHTML="Kliknij aby zacząć zbierać grzyby !";
    mushroom = '';
    mushrooms = ["borowik","kania","kozlarz","maslak","rydz","muchomor"];
    how_many_gathered = 0;
}